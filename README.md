# Space Invaders

@authors
Corentin BOSQUET - Quentin GALLIOU - IMR1 - ENSSAT

Application Java utilisant JavaFx.

## But

Cette application est un space invaders. Le but est de tuer tous les aliens sans que eux vous tue.
Bonne chance.

## Cadre
Cette application a été développé dans le cadre d'un projet Java pour l'ENSSAT.

***Attention :*** Après Java8, Javafx ne fait plus partie du jre. Il est à installer ! ::warning::

## Installation et lancement
Le projet a été codé en Java11. Il faut donc ajouter les libs Javafx.
Elles sont disponible à l'adresse : https://openjfx.io/openjfx-docs/

**Pour exécuter le jar :**
Il se trouve à l'emplacement : spaceinvaders/spaceinvaders.jar

Il faudra lancer la commande : java -jar --module-path [Emplacement des lib javafx] --add-modules javafx.controls,javafx.fxml SpaceInvaders.jar
