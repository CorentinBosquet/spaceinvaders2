package views;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Classe de construction de la page Menu
 */
public class Menu {
    private static Menu menu = new Menu();
    private Scene scene_menu;
    private Pane pane_menu = new Pane();
    private Button nouvelle_partie = new Button("Nouvelle partie ?");
    private Button options = new Button("Options");
    private Button fermer_appli = new Button("Fermer l'application");
    private VBox vBox = new VBox();

    /**
     * Constructeur
     */
    private Menu() {
        construction_menu();
        this.scene_menu = new Scene( this.pane_menu );
    }
    
    /**
     * Récupération de l'instance Menu
     * @return menu : Menu
     */
    public static Menu getMenu() {
        return menu;
    }

    /**
     * Permet de construuire l'interface
     */
    private void construction_menu() {
        vBox.setAlignment(Pos.CENTER);
        vBox.setPrefSize(800, 700);
        vBox.setSpacing(50);
     
        Label titre = new Label("Space Invaders");
        titre.setTextFill(Color.web("#FFFFFF"));
        titre.setStyle("-fx-font-size: 40;");
        
        fermer_appli.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
        
        options.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
        
        nouvelle_partie.setStyle(
        		"-fx-font-size: 20;" +
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
 
        vBox.getChildren().addAll(titre, nouvelle_partie, options, fermer_appli);
        
        pane_menu.getChildren().add(vBox);
        pane_menu.setStyle("-fx-background-color: #000066");
    }
    
    /**
     * Permet de récupérer la scene
     * @return scene_menu : scene
     */
    public Scene getScene() {
    	return this.scene_menu;
    }
    
    /**
     * Récupération de l'element btn_nouvelle_partie
     * @return btn_nouvelle_partie : Button
     */
    public Button getBtnNouvellePartie() {
    	return this.nouvelle_partie;
    }
    
    /**
     * Récupération de l'element btn_options
     * @return btn_options : Button
     */
    public Button getBtnOptions() {
    	return this.options;
    }
    
    /**
     * Récupération de l'element btn_fermer
     * @return btn_fermer : Button
     */
    public Button getBtnFermer() {
    	return this.fermer_appli;
    }
}