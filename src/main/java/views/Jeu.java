package views;

import controller.AppliSI;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import models.SpaceCanvas;

/**
 * Classe de construction de l'interface Jeu
 */
public class Jeu {
    private static Jeu jeu = new Jeu();
    
    private Scene scene_jeu;
    private BorderPane pane_jeu = new BorderPane();
    private Canvas canvas = SpaceCanvas.getSpaceCanvas().getCanvas();
    private VBox vb_informations = new Informations(AppliSI.getControleInfo()).getInformations();

    /**
     * Constructeur
     */
    public Jeu() {
    	construction_jeu();
        this.scene_jeu = new Scene( this.pane_jeu );
    }
    
    /**
     * Permet de récupérer l'instance de classe
     * @return jeu : Jeu
     */
    public static Jeu getJeu() {
        return jeu;
    }

    /**
     * Permet de construire l'interface Jeu
     */
    public void construction_jeu() {
    	BackgroundImage myBI= new BackgroundImage(new Image("images/fond.png",800,700,false,true),
		        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		          BackgroundSize.DEFAULT);
		pane_jeu.setBackground(new Background(myBI));
        pane_jeu.setLeft(canvas);
        pane_jeu.setRight(vb_informations);
    }
    
    /**
     * Permet de récupérer la scene de l'interface
     * @return scene_jeu : Scene
     */
    public Scene getScene() {
    	return this.scene_jeu;
    }
    
    /**
     * Permet de récupérer le contenu de la scene
     * @return pane_jeu : Pane
     */
    public BorderPane getPane() {
    	return this.pane_jeu;
    }
    
    /**
     * Permet de récupérer le canvas de l'interface
     * @return canvas : Canvas
     */
    public Canvas getCanvas() {
    	return this.canvas;
    }
}