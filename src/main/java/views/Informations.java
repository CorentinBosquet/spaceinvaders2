package views;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import models.InfoJeu;

public class Informations {
	
private VBox ecran_info = new VBox();
	
	private Label lbl_score = new Label();
	private Label lbl_best_score = new Label();
	private Label lbl_life = new Label();
	
	/**
	 * Constructeur
	 * @param info : contient les informations de la partie en cours
	 */
	public Informations(InfoJeu info) {
		lbl_score.setText("SCORE  " + info.getScore());
		lbl_score.setTextFill(Color.web("#FFFFFF"));
		  
		lbl_best_score.setText("BEST SCORE  " + info.getHighScore());
		lbl_best_score.setTextFill(Color.web("#FFFFFF"));
		  
		lbl_life.setText("VIES  " + info.getVie());
		lbl_life.setTextFill(Color.web("#FFFFFF"));
		
		ecran_info.setPadding(new Insets(10,10,10,10));
		ecran_info.setSpacing(20);
		ecran_info.getChildren().addAll(lbl_score, lbl_best_score, lbl_life);
	}
	
	/**
	 * Permet de récupérer le VBox avec les nouvelles données
	 * @return ecran_info
	 */
	public VBox getInformations(){
		return this.ecran_info;
	}

}
