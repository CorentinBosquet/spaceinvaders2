package views;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Permet de créer une scene avec les options
 */
public class Options {
    private static Options options = new Options();
    
    private Scene scene_option;
    private Pane pane_option = new Pane();

    private Label lbl_fond = new Label("Fond");
    private Button btn_fond = new Button("Selectionner une image");

    private Label lbl_alien = new Label("Alien");
    private Button btn_alien = new Button("Selectionner une image");
    
    private Label lbl_nb_alien = new Label("Nombre d'Alien");
    private Slider sl_nb_alien = new Slider();
    private Label valeur_nb = new Label("");
    
    private Label lbl_vi_alien = new Label("Vitesse des aliens");
    private Slider sl_vi_alien = new Slider();
    private Label valeur_vi = new Label("");

    private Button btn_valider = new Button("Valider");

    /**
     * Constructeur 
     */
    private Options() {
        construction_options();
        this.scene_option = new Scene( this.pane_option );
    }
    
    /**
     * Recupération de l'instance options
     * @return options : instance de classe
     */
    public static Options getOptions() {
        return options;
    }

    /**
     * Créée l'interface option
     */
    private void construction_options() {
        VBox vBox = new VBox();
        	
        sl_nb_alien.setMin(1);
        sl_nb_alien.setMax(5);
        sl_nb_alien.setBlockIncrement(1);
        sl_nb_alien.setValue(5);
        valeur_nb.setText(String.valueOf(sl_nb_alien.getValue()));
        
        sl_vi_alien.setMin(25);
        sl_vi_alien.setMax(100);
        sl_vi_alien.setBlockIncrement(5);
        sl_vi_alien.setValue(50);
        valeur_vi.setText(String.valueOf(sl_vi_alien.getValue()));
        
        HBox hb_fond_alien = new HBox(lbl_alien, btn_alien);
        HBox hb_fond_fond = new HBox(lbl_fond, btn_fond);
        HBox hb_nb_alien = new HBox(lbl_nb_alien, sl_nb_alien, valeur_nb);
        HBox hb_vi_alien = new HBox(lbl_vi_alien, sl_vi_alien, valeur_vi);
        
        hb_fond_alien.setSpacing(10);
        hb_fond_alien.setAlignment(Pos.CENTER);
        hb_fond_fond.setSpacing(10);
        hb_fond_fond.setAlignment(Pos.CENTER);
        hb_nb_alien.setSpacing(10);
        hb_nb_alien.setAlignment(Pos.CENTER);
        hb_vi_alien.setSpacing(10);
        hb_vi_alien.setAlignment(Pos.CENTER);
        
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(20);
        vBox.setPrefSize(800, 700);
        vBox.getChildren().addAll(hb_fond_fond, hb_fond_alien, hb_nb_alien, hb_vi_alien, btn_valider);
 
        lbl_alien.setTextFill(Color.web("#FFFFFF"));
        lbl_fond.setTextFill(Color.web("#FFFFFF"));
        lbl_nb_alien.setTextFill(Color.web("#FFFFFF"));
        lbl_vi_alien.setTextFill(Color.web("#FFFFFF"));
        valeur_nb.setTextFill(Color.web("#FFFFFF"));
        valeur_vi.setTextFill(Color.web("#FFFFFF"));
        
        pane_option.getChildren().add(vBox);
        pane_option.setStyle("-fx-background-color: #000066");
        
        btn_alien.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
        
        btn_fond.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
        
        btn_valider.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
    }
    
    /**
     * Récupération de l'element btn_valider
     * @return btn_valider : Button
     */
    public Button getBtn_valider() {
		return btn_valider;
	}
    
    /**
     * Récupération de l'element btn_fond
     * @return btn_fond : Button
     */
    public Button getBtn_fond() {
		return btn_fond;
	}
    
    /**
     * Récupération de l'element btn_alien
     * @return btn_alien : Button
     */
    public Button getBtn_alien() {
		return btn_alien;
	}
    
    /**
     * Retourne le sliders
     * @return sliders
     */
    public Slider getSliderNb() {
    	return this.sl_nb_alien;
    }
    
    /**
     * Retourne le sliders
     * @return sliders
     */
    public Slider getSliderVi() {
    	return this.sl_vi_alien;
    }

    /**
     * Récupération de la scene
     * @return scene_option : scene
     */
	public Scene getScene() {
    	return this.scene_option;
    }
	
	/**
	 * Récupération des labels valeurs des sliders
	 */
	public Label getLabelNb() {
		return this.valeur_nb;
	}
	
	/**
	 * Récupération des labels valeurs des sliders
	 */
	public Label getLabelVi() {
		return this.valeur_vi;
	}
}