package views;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * Permet de construire l'interface en cas d'échec
 */
public class FinPartie {

    private static FinPartie finPartie = new FinPartie();
    private Scene scene_finPartie;
    private Pane pane_finPartie = new Pane();
    private Button fermer_jeu = new Button("Retour menu");
    private Label lbl_fin = new Label("Vous avez perdu !");

    /**
     * Constructeur
     */
    private FinPartie() {
    	construction_fin_partie();
        this.scene_finPartie = new Scene( this.pane_finPartie );
    }
    
    /**
     * Permet de récupérer l'instance de classe
     * @return finPartie : FinPartie
     */
    public static FinPartie getFin() {
        return finPartie;
    }

    /**
     * Permet de construire l'interface
     */
    private void construction_fin_partie() {
        VBox vBox = new VBox();
        vBox.setPrefSize(800, 700);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(20);
        
        lbl_fin.setTextFill(Color.web("#FFFFFF"));
        lbl_fin.setStyle("-fx-font-size: 40;");
        vBox.getChildren().addAll(lbl_fin, fermer_jeu);
        
        pane_finPartie.getChildren().add(vBox);
        pane_finPartie.setStyle("-fx-background-color: #000066");
        
        fermer_jeu.setStyle(
        		"-fx-background-color:" + 
        				"linear-gradient(#f2f2f2, #d6d6d6)," + 
        				"linear-gradient(#fcfcfc 0%, #d9d9d9 20%, #d6d6d6 100%)," + 
        				"linear-gradient(#dddddd 0%, #f6f6f6 50%);" + 
        		"-fx-background-radius: 8,7,6;" + 
        		"-fx-background-insets: 0,1,2;" + 
        		"-fx-text-fill: black;" + 
        		"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );"
        );
    }
    
    /**
     * Permet de récupérer la scene de l'interface
     * @return scene_finPartie : Scene
     */
    public Scene getScene() {
    	return this.scene_finPartie;
    }
    
    /**
     * Récupération de l'element btn_fermer
     * @return btn_fermer : Button
     */
    public Button getBtn_Fermer() {
    	return this.fermer_jeu;
    }
}