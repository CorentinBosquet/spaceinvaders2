package models;

/**
 * Classe de création de tir
 */
public class Bullet extends Mouvements {

	/**
	 * Constructeur de Bullet
	 * @param x : position de l'element
	 * @param y : position de l'element
	 * @param sprite : element
	 * @param larg : largeur de l'element
	 * @param haut : hauteur de l'element
	 * @param yVitesse : vitesse de l'element
	 */
    private Bullet(int x, int y, Sprite sprite, int larg, int haut, int yVitesse) {
        super(x, y, sprite, larg, haut, 0, yVitesse);
    }

    /**
     * Permet de récupérer un tir
     * @param x
     * @param y
     * @param yVitesse
     * @return
     */
    public static Bullet getBullet(int x, int y, int yVitesse) {
        Sprite sprite = new Sprite("/images/composants/bullet.gif", 12, 23);
        return new Bullet(x, y, sprite, sprite.getLarg(), sprite.getHaut(), yVitesse);
    }
}
