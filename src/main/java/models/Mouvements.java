package models;

/**
 * Permet de bouger les elements
 */
public abstract class Mouvements {
    private int x;
    private int y;
    private Sprite sprite;
    private int larg;
    private int haut;
    private double xVitesse;
    private int yVitesse;

    private SpaceCanvas spaceCanvas = SpaceCanvas.getSpaceCanvas();

    /**
     * Constructeur de la classe Mouvement
     * @param x : position de l'élément
     * @param y : position de l'élément
     * @param sprite : element à bouger
     * @param larg : largeur de l'élément
     * @param haut : hauteur de l'élément
     * @param xVitesse : vitesse de l'élément
     * @param yVitesse : vitesse de l'élément
     */
    public Mouvements(int x, int y, Sprite sprite, int larg, int haut, double xVitesse, int yVitesse) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.larg = larg;
        this.haut = haut;
        this.xVitesse = xVitesse;
        this.yVitesse = yVitesse;
    }

    /**
     * Permet de se déplacer vers le haut
     */
    public void haut() {
        spaceCanvas.effacer(this);
        this.y -= this.getYVitesse();
        spaceCanvas.dessiner(this);
    }

    /**
     * Permet de se déplacer vers la droite
     */
    public void droite() {
        spaceCanvas.effacer(this);
        this.x += this.getXVitesse();
        spaceCanvas.dessiner(this);
    }

    /**
     * Permet de se déplacer vers le bas
     */
    public void bas() {
        spaceCanvas.effacer(this);
        this.y += this.getYVitesse();
        spaceCanvas.dessiner(this);
    }

    /**
     * Permet de se déplacer vers la gauche
     */
    public void gauche() {
        spaceCanvas.effacer(this);
        this.x -= this.getXVitesse();
        spaceCanvas.dessiner(this);
    }

    /**
     * Permet de récupérer la position X
     * @return x : position X
     */
    public int getX() {
        return x;
    }

    /**
     * Permet de modifier la position X
     * @param x : position x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Permet de récupérer la position Y
     * @return y : position Y
     */
    public int getY() {
        return y;
    }

    /**
     * Permet de modifier la position Y
     * @param y : position y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Permet de récupérer l'element
     * @return sprite : element
     */
    public Sprite getSprite() {
        return sprite;
    }

    /** 
     * Permet de récupérer la largeur de l'element
     * @return larg : largeur de l'élément
     */
    public int getLarg() {
        return larg;
    }

    /**
     * Permet de modifier la largeur de l'element
     * @param larg : largeur
     */
    public void setLarg(int larg) {
        this.larg = larg;
    }

    public int getHaut() {
        return haut;
    }

    public void setHaut(int haut) {
        this.haut = haut;
    }

    /**
     * Permet de récupérer la vitesse
     * @return xVitesse : vitesse de l'élément
     */
    public double getXVitesse() {
        return xVitesse;
    }

    /**
     * Permet de récupérer la vitesse
     * @return yVitesse : vitesse de l'élément
     */
    public int getYVitesse() {
        return yVitesse;
    }

}
