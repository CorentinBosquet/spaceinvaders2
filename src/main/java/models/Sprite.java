package models;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Cette classe permet de modifier l'image des éléments lors des déplacements
 */
public class Sprite {
    private Image image;
    private ImageView imageView;
    private int larg;
    private int haut;
 
    /**
     * Constructeur de Sprite
     * @param p : Emplacement de l'image de l'élément
     * @param l : Largeur de l'image
     * @param h : Hauteur de l'image
     */
    public Sprite(String p, int l, int h) {
        this.larg = l;
        this.haut = h;
        this.image = new Image(p);
        this.imageView = new ImageView(image);
    }

    /**
     * Permet de récupérer l'image de l'élément
     * @return image : image de l'élément
     */
    public Image getImage() {
        return image;
    }

    /**
     * Permet de récupérer l'imageView de l'élément
     * @return imageView : imageView de l'élément
     */
    public ImageView getImageView() {
        return imageView;
    }

    /**
     * Permet de récupérer la largeur de l'image
     * @return larg : largeur de l'image
     */
    public int getLarg() {
        return larg;
    }

    /**
     * Permet de modifier la largeur de l'image
     * @param l : largeur de l'image
     */
    public void setLarg(int l) {
        this.larg = l;
    }

    /**
     * Permet de récupérer la hauteur de l'image
     * @return haut : hauteur de l'image
     */
    public int getHaut() {
        return haut;
    }

    /**
     * Permet de modifier la hauteur de l'image
     * @param h : hauteur de l'image
     */
    public void setHaut(int h) {
        this.haut = h;
    }
}
