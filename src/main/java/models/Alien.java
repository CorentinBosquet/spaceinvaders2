package models;

/**
 * Classe de gestion des aliens
 */
public class Alien extends Mouvements {

	/**
	 * Constructeur de Alien
	 * @param x : position de l'element
	 * @param y : position de l'element
	 * @param sprite : element
	 * @param larg : largeur de l'element
	 * @param haut : hauteur de l'element
	 * @param xVitesse : vitesse de l'element
	 */
    private Alien(int x, int y, Sprite sprite, int larg, int haut, double xVitesse) {
        super(x, y, sprite, larg, haut, xVitesse, 10);
    }

    /**
     * Permet de récupérer un alien
     * @param x : position de l'element
     * @param y : position de l'element
     * @param xVitesse : vitesse de l'element
     * @return alien : alien
     */
    public static Alien alien(int x, int y, double xVitesse) {
        Sprite sprite = new Sprite("/images/composants/alien.gif", 43, 29);
        return new Alien(x, y, sprite, sprite.getLarg(), sprite.getHaut(), xVitesse);
    }

}
