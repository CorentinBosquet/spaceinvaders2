package models;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * Classe qui permet de déssiner sur la canvas
 */
public class SpaceCanvas {
	
    private static SpaceCanvas spaceCanvas = new SpaceCanvas();
    private Canvas canvas;
    private GraphicsContext graphicsContext;

    /**
     * Constructeur de SpaceCanvas
     */
    private SpaceCanvas() {
        this.canvas = new Canvas(600, 650);
        this.graphicsContext = canvas.getGraphicsContext2D();
    }
    
    /**
     * Permet de récupérer une instance de SpaceCanvas
     * @return spaceCancas : instance de la classe
     */
    public static SpaceCanvas getSpaceCanvas() {
        return spaceCanvas;
    }

    /**
     * Permet d'effacer une element précis sur la canvas
     * @param element : composant du canvas
     */
    public void effacer(Mouvements element) {
        graphicsContext.clearRect(
        		element.getX(),
        		element.getY(),
                element.getLarg(),
                element.getHaut()
        );
    }

    /**
     * Permet de dessiner un element sur le canvas
     * @param element : composant du canvas
     */
    public void dessiner(Mouvements element) {
        graphicsContext.drawImage(
        		element.getSprite().getImage(),
                element.getX(),
                element.getY()
                
        );
    }

    /**
     * Permet de récupérer le canvas de jeu
     * @return canvas : canvas de jeu
     */
    public Canvas getCanvas() {
        return canvas;
    }
}
