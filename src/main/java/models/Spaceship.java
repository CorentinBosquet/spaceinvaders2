package models;

/**
 * Classe de création du vaisseau
 */
public class Spaceship extends Mouvements {

	/**
	 * Constructeur de Spaceship
	 * @param x : position du vaisseau
	 * @param y : position du vaisseau
	 * @param sprite : image du vaisseau
	 * @param larg : largeur du vaisseau
	 * @param haut : hauteur du vaisseau
	 * @param vitesse : vitesse de déplacement 
	 */
    private Spaceship(int x, int y, Sprite sprite, int larg, int haut, int vitesse) {
        super(x, y, sprite, larg, haut, vitesse, 0);
    }

    /**
     * Permet de récupérer un vaisseau
     * @param x : position du vaisseau
     * @param y : position du vaisseau
     * @param vitesse : vitesse du vaisseau
     * @return spaceship : un nouveau vaisseau
     */
    public static Spaceship getSpaceship(int x, int y, int vitesse) {
        Sprite sprite = new Sprite("/images/composants/ship.gif", 33, 23);
        return new Spaceship(x, y, sprite, sprite.getLarg(), sprite.getHaut(), vitesse);
    }

}
