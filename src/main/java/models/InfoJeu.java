package models;

import java.util.Observable;
import java.util.Observer;

/**
 * Permet de traiter les information de la partie
 */
@SuppressWarnings("deprecation")
public class InfoJeu extends Observable {
    private int highScore;
    private int score;
    private int vie;

    /**
     * Constructeur de infoJeu
     * @param highScore : meilleur score enregistré
     */
    public InfoJeu(int highScore, int score, int vie) {
        this.highScore = highScore;
        this.score = score;
        this.vie = vie;
    }

    /**
     * Retourne le meilleur score
     * @return highscore
     */
    public int getHighScore() {
        return highScore;
    }

    /**
     * Modifie le meilleur score
     * @param highScore
     */
    public void setHighScore(int highScore) {
        this.highScore = highScore;
        notifyView();
    }
    
    /**
     * Retourne le  score
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * Modifie le score
     * @param score
     */
    public void setScore(int score) {
        this.score = score;
        notifyView();
    }
    
    /**
     * Récupère le nombre de vie
     * @return vie
     */
    public int getVie() {
    	return vie;
    }
    
    /**
     * Modifie le nombre de vie
     * @param vie
     */
    public void setVie(int vie) {
    	this.vie = vie;
    	notifyView();
    }
    
    /**
	 * Permet d'ajouter un observer à la partie
	 * @param vue
	 */
	public void addView(Observer vue) {
		addObserver(vue);
	}
	
	/**
	 * Permet de notifier tous les observer de la partie
	 */
	public void notifyView() {
		// Permet de notifier qu'il y a un changement à toutes les vues liées
		setChanged();
		notifyObservers(this);
	}
}
