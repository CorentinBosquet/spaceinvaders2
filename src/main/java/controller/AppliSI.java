package controller;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import models.*;
import views.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Permet de gérer le fonctionnement de l'application
 */
@SuppressWarnings("deprecation")
public class AppliSI extends Application implements Observer {
	
	private static InfoJeu infoGame = new InfoJeu(0, 0, 3);
	// VBox qui contient le score, highscore et le nombre de vie
	private VBox vb_informations = new Informations(infoGame).getInformations();
	
	// L'application est composé de 4 modules
    private Menu menu = Menu.getMenu();
    private Jeu jeu = Jeu.getJeu();
    private Options options = Options.getOptions();
    private FinPartie fin = FinPartie.getFin();
    
    private Stage stage;

    private SpaceCanvas spaceCanvas = SpaceCanvas.getSpaceCanvas();
    private Canvas canvas = spaceCanvas.getCanvas();

    private Spaceship vaisseau;
    private List<Alien> mAliens;
    private List<Bullet> mBullets;

    private AnimationTimer animationTimer;

    // Valeurs par défaut. Peuvent être modifié dans options
    private double ligne_aliens = 5;
    private double col_aliens = 5;
    private double vi_alien = 50;
    
    
    private String f_background = "";
    private boolean pause = false;

    /**
     * Permet de créer l'interface graphique
     * @param stage : Interface
     */
    @Override
    public void start(Stage stage) {

        this.stage = stage;
        this.stage.getIcons().add(new Image("images/space-invader-icon.png"));

        //Gestion des boutons
        menu();
        options();
        
        // Ajout d'une vue pour le modele Observable - Observer
        infoGame.addView(this);   
        
        keyboardEvents( menu.getScene() );
        keyboardEvents( jeu.getScene() );
        keyboardEvents( options.getScene() );
                
        Scene scene = menu.getScene();
        stage.setTitle("SpaceInvaders");
        stage.setWidth(800);
        stage.setHeight(700);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Permet de préparer le tableau de jeu avant de lancer la partie
     */
    private void init_jeu() {
        // Récupère le hishscore, stocké dans un fichier
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./sauvegarde.txt"));
			String bs = reader.readLine();
			reader.close();
			infoGame.setHighScore(Integer.parseInt(bs));
		} catch (IOException e) {
			e.printStackTrace();
		}
        infoGame.setScore(0);
        infoGame.setVie(3);
        
        // On efface tous sur le plateau de jeu (spaceCanvas)
        if(mAliens != null){
            for(Alien alien: mAliens){
                spaceCanvas.effacer(alien);
            }
        }
        if(mBullets != null){
            for(Bullet bullet: mBullets){
                spaceCanvas.effacer(bullet);
            }
        }
        if(vaisseau != null) {
            spaceCanvas.effacer(vaisseau);
        }

        this.vaisseau = null;
        this.mAliens = new ArrayList<>();
        this.mBullets = new ArrayList<>();

        // On crée les éléments de jeu
        creerVaisseau();
        creerAliens(ligne_aliens, col_aliens, vi_alien);

        // animationTimer est notre boucle de jeu
        this.animationTimer = new AnimationTimer() {
            private long majAliens = 0;
            private boolean check = true;

            @Override
            public void handle(long now) {
            	// Permet de faire monter les tirs
                mouvementTirs();

                // Permer de bouger les aliens
                if (now - majAliens >= 28_000_000 * 10) {
                	check = mouvementAliens(this.check);
                	majAliens = now ;
                }
                
                // On vérifie s'il y a collision
                check_collision(now);

                // On efface les aliens à leur position pércédentes
                // et on les redessine 
                for (Alien alien: mAliens) {
                    spaceCanvas.effacer(alien);
                    spaceCanvas.dessiner(alien);
                }

                // S'il n'y a plus d'alien le joueur a gagné
                // Sinon, il a perdu
                if(mAliens.isEmpty()) {
                	gagne();
                } else {
                	gameOver();
                }
            }
        };
        animationTimer.start();
    }

    /**
     * Permet de deplacer les aliens à droite
     * @param check_droit
     * @return check_droit : permet de vérifier qu'ils peuvent encore bouger
     */
    private boolean mouvementAliens(boolean check_droit) {
    	// On ne fait pas apparraître des aliens qui seraient hors du plateau de jeu
        for ( Alien alien: mAliens ) {
            if ( alien.getX() >= canvas.getWidth() - alien.getLarg() ) {
            	basAliens();
                check_droit = false;
            }
            else if ( alien.getX() <= alien.getLarg() ) {
                check_droit = true;
            }

            // S'il y a de la place à droite, ils s'y deplacent
            if( check_droit ) { 
            	alien.droite(); 
            } else { 
            	alien.gauche(); 
            }
        }

        return check_droit;
    }

    /**
     * Permet de deplacer les aliens en bas
     */
    private void basAliens() {
        for ( Alien lAlien: mAliens ) {
            lAlien.bas();
        }
    }

    /**
     * Permet de détecter une collision
     * @param now : tempds du timer
     */
    private void check_collision(long now) {

        for( int indexBullet = 0; indexBullet < mBullets.size(); indexBullet++ ) {
            Bullet lBullet = mBullets.get(indexBullet);
            for( int indexAlien = mAliens.size()-1; indexAlien >= 0; indexAlien-- ) {
                Alien lAlien = mAliens.get(indexAlien);

                // Si le missile et un alien ont les mêmes coordonnées, il y a une colision
                if(lBullet.getX() >= lAlien.getX() - lAlien.getLarg()/2 && lBullet.getX() <= lAlien.getX() + lAlien.getLarg()){
                    if(lBullet.getY() <= lAlien.getY()) {

                        spaceCanvas.effacer(lBullet);
                        spaceCanvas.effacer(lAlien);

                        mBullets.remove(indexBullet);
                        mAliens.remove(indexAlien);
                        infoGame.setScore(infoGame.getScore()+100);
                        break;
                    }
                }
            }
        }

        // Lorsque les aliens arrivent au niveau du vaissuea, la partie est finie
        for(Alien alien: mAliens){
            if(alien.getY() + alien.getHaut() >= vaisseau.getY()){
                spaceCanvas.effacer(alien);
                spaceCanvas.effacer(vaisseau);
                break;
            }
        }
    }

    /**
     * Permet de bouger les tirs
     */
    private void mouvementTirs() {
    	// Pour chaque missile, on le fait monter jusqu'à ce qu'il arrive en haut
    	// S'il touche un alien, il est tout de suite supprimé
        for( int indexBullet = 0; indexBullet < mBullets.size(); indexBullet++ ) {
            Bullet bullet = mBullets.get(indexBullet);
            if( bullet.getY() > 0 ) {
                bullet.haut();
            } else {
                spaceCanvas.effacer(bullet);
                mBullets.remove(indexBullet);
                break;
            }
        }
    }

    /** 
     * Cas où les aliens gagnent
     */
    private void gameOver() {
    	// Si les aliens gagnent, une page de fin de partie s'affiche
        for(Alien lAlien: mAliens){
            if(lAlien.getY() + lAlien.getHaut() >= vaisseau.getY()){
                spaceCanvas.effacer(lAlien);
                spaceCanvas.effacer(vaisseau);
                animationTimer.stop();
                stage.setScene( fin.getScene() );
            }
        }
    }
    
    /** 
     * Cas où le joueur gagne
     */
    private void gagne() {
    	// Lorsqu'il n'y a plus de joueur, on enregistre le score et on
    	//redirige le joueur vers l'accueil
		animationTimer.stop();
		// Si le score est plus grand que le highscore, on l'enregistre
		if(infoGame.getScore() > infoGame.getHighScore()) {
			FileWriter fileWriter;
			try {
				fileWriter = new FileWriter("./sauvegarde.txt");
				PrintWriter printWriter = new PrintWriter(fileWriter);
			    printWriter.print(infoGame.getScore());
			    printWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		
		// On affiche le score dans une fenetre
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Félicitation !");
		alert.setHeaderText("Vous avez gagné !");
		alert.setContentText("Votre score : " + infoGame.getScore());

		alert.show();
		
		stage.setScene( menu.getScene() );
    }
     

    /**
     * Liste et traite tous les listenners
     * @param scene : Scene
     */
    private void keyboardEvents(Scene scene){

        scene.setOnKeyPressed(e -> {
            switch (e.getCode()){
                case LEFT:
                    if( vaisseau != null && vaisseau.getX() >= 0  ) {
                        vaisseau.gauche();
                    }
                    break;
                case RIGHT:
                    if( vaisseau != null && vaisseau.getX() <= canvas.getWidth() - vaisseau.getHaut()  ) {
                        vaisseau.droite();
                    }
                    break;
                case SPACE:
                    if ( vaisseau != null ) {
                        creerMissile();
                        infoGame.setScore(infoGame.getScore()-25);
                    }
                    break;
                case P:
                	if(pause == false) {
                		animationTimer.stop();
                		pause = true;
                	} else {
                		animationTimer.start();
                		pause = false;
                	}
                    break;
				default:
					break;
	            }
        });
    }

    /**
     * Permet de créer un vaisseau
     */
    private void creerVaisseau() {
    	// On parametre le vaisseau et on l'affiche
    	// Il n'est ensuite plus modifié
        vaisseau = Spaceship.getSpaceship((int) canvas.getWidth() / 2, (int) canvas.getHeight(), 10);
        vaisseau.getSprite().setLarg( vaisseau.getSprite().getLarg() / 2 );
        vaisseau.setLarg( vaisseau.getLarg() * 2 );
        vaisseau.setHaut( vaisseau.getHaut() * 2 );
        vaisseau.setX( vaisseau.getX() - vaisseau.getLarg() / 2 );
        vaisseau.setY( vaisseau.getY() - vaisseau.getHaut());
        spaceCanvas.dessiner(vaisseau);
    }

    /**
     * Permet de créer une liste d'alien
     * @param ligne_alien : nombre d'alien
     * @param col_alien : nombre d'alien
     * @param vit_alien : vitesse des aliens
     */
    private void creerAliens(double ligne_alien, double col_alien, double vit_alien) {
        int origineX = 0;
        int x = origineX;
        int y = 0;

        int alienHauteur = 0;

        for( int iColumn = 0; iColumn < ligne_alien; iColumn++ ) {
            for( int iRow = 0; iRow < ligne_alien; iRow++ ) {
                Alien alien = Alien.alien(x, y, vit_alien);
                mAliens.add(alien);
                
                alienHauteur = alien.getHaut();
                x += alien.getLarg() + 0;
            }
            x = origineX;
            y += alienHauteur + 10;
        }

        // On dessine la liste des aliens
        for (Alien alien: mAliens) {
            spaceCanvas.dessiner(alien);
        }
    }

    /**
     * Permet de créer un missile
     */
    private void creerMissile() {
        Bullet bullet = Bullet.getBullet(
                vaisseau.getX() + vaisseau.getLarg() / 5,
                vaisseau.getY() - vaisseau.getHaut()+20,
                4
        );
        
        mBullets.add(bullet);
        spaceCanvas.dessiner(bullet);
    }



	/**
	 * Permet d'initialiser le menu
	 */
    private void menu() {
    	// Commence une partie
        menu.getBtnNouvellePartie().setOnAction( actionEvent -> {
        	init_jeu();
            animationTimer.start();
            stage.setScene( jeu.getScene() );
        });

        //Options
        menu.getBtnOptions().setOnAction( actionEvent -> {
            stage.setScene( options.getScene() );
        });

        //Exit
        menu.getBtnFermer().setOnAction( actionEvent -> System.exit(0) );
        fin.getBtn_Fermer().setOnAction(actionEvent -> {
        	stage.setScene(menu.getScene());
        });
    }

    /**
     * Permet d'initialiser les options
     */
    private void options() {
    	// L'image des aliens peut être modifiée
    	options.getBtn_alien().setOnAction(event ->{
    		 FileChooser fc_alien = new FileChooser();
    		 fc_alien.setTitle("Image des aliens");
    		 File file = fc_alien.showOpenDialog(stage);
    		 if (file != null) {
                 setBackground(file);
                 options.getBtn_alien().setText(file.getName());
             }
    	});
    	
    	// La valeur du nombre d'aliens peut être modifiée
    	options.getSliderNb().valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
	                  	options.getLabelNb().setText(String.valueOf((int)new_val.doubleValue()));
	                  	ligne_aliens = new_val.doubleValue();
	                  	col_aliens = new_val.doubleValue();
            }});
    	
    	// La valeur de la vitesse des aliens peut être modifiée
    	options.getSliderVi().valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
	                  	options.getLabelVi().setText(String.valueOf((int)new_val.doubleValue()));
	                  	vi_alien = new_val.doubleValue();
            }});
    	
    	// L'image de fond peut être modifiée
    	options.getBtn_fond().setOnAction(event ->{
    		 FileChooser fc_fond = new FileChooser();
    		 fc_fond.setTitle("Image du fond");
    		 File file = fc_fond.showOpenDialog(stage);
    		 if (file != null) {
                 setBackground(file);
                 options.getBtn_fond().setText(file.getName());
             }
    	});
    	
    	// Permet de valider la configuration
        options.getBtn_valider().setOnAction( actionEvent -> {
            majParametre();
            init_jeu();
            stage.setScene( jeu.getScene() );
        });     
    }
    
    /**
     * Permet de définir un fond
     * @param file : emplacement de l'image
     */
    private void setBackground(File file) {
    	f_background = file.getAbsolutePath();
    }
    
    /**
     * Permet de récupérer le fond
     * @return fond : fond de l'application
     */
    private String getBackground() {
    	return f_background;
    }

    /**
     * Met à jour les nouveaux paramètres
     */
    private void majParametre() {
    	// On applique le nouveau fond de jeu
    	if(getBackground() != "") {
    		BackgroundImage myBI= new BackgroundImage(new Image("file:"+getBackground(),800,700,false,true),
    		        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
    		          BackgroundSize.DEFAULT);
    		jeu.getPane().setBackground(new Background(myBI));
    	}
    }
    
    /**
     * Permet de mettre les informations à jour
     */
    @Override
	public void update(Observable  arg0, Object arg1) {
    	InfoJeu info = (InfoJeu) arg1;
    	vb_informations = new Informations(info).getInformations();
		jeu.getPane().setRight(vb_informations);
	}
    
    /**
     * Permet de récupérer les nouvelles infos
     * @return infogae : InfoJeu
     */
    public static InfoJeu getControleInfo() {
    	return infoGame;
    }

    /**
     * Lance l'application
     * @param args : String[]
     */
    public static void main(String[] args) {
        launch(args);
    }
}
