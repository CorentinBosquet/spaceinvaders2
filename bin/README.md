# Space Invaders

@authors
Corentin BOSQUET - Quentin GALLIOU - IMR1 - ENSSAT

Application Java utilisant JavaFx.

## But

Cette application est un space invaders. Le but est de tuer tous les aliens sans que eux vous tue.
Bonne chance.

## Cadre
Cette application a été développé dans le cadre d'un projet Java pour l'ENSSAT.

***Attention : *** Après Java8, Javafx ne fait plus partie du jre. Il est à installer ! ::warning::

